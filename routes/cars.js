const express = require('express');
const router = express.Router();
const moment = require('moment');
const Car = require('../models/car');


router.get('/', (req, res) => {
   Car.find({}).exec((error, cars) => {
      if (error) {
         res.status(500).send(error.message);
      } else {
         console.log(cars);
         res.send(cars.map(car => mapToCar(car)))
      }
   });
})















// router.get('/', (req, res) => {
//   Book.find({}).exec((error, books) => {
//     if (error) {
//       res.status(500).send(error.message);
//     } else {
//       console.log(books);
//       res.send(books.map(book => mapToBook(book)));
//     }
//   });
// });

// router.get('/bestsellers', (req, res) => {
//   Book.find({ bestSeller: true }).exec((error, books) => {
//     if (error) {
//       res.status(500).send(error.message);
//     } else {
//       console.log(books);
//       res.send(books);
//     }
//   });
// });

// router.get('/:id', (req, res) => {
//   Book.findOne({ _id: req.params.id }).exec((error, book) => {
//     if (error) {
//       res.status(500).send(error.message);
//     } else {
//       res.send(mapToBook(book));
//     }
//   });
// });

// router.post('/', (req, res) => {
//   console.log('sukuriam nauja knyga');
//   const newBook = new Book();

//   newBook.title = req.body.title;
//   newBook.author = req.body.author;
//   newBook.year = req.body.year;
//   newBook.category = req.body.category;
//   newBook.bestSeller = req.body.bestSeller;

//   newBook.save((error, book) => {
//     if (error) {
//       res.status(500).send(error.message);
//     } else {
//       console.log(book);
//       res.status(201).send(book);
//     }
//   });
// });

// router.put('/:id', (req, res) => {
//   Book.findByIdAndUpdate(
//     { _id: req.params.id },
//     {
//       $set: Object.assign({}, req.body),
//     },
//     { new: true },
//     (error, book) => {
//       if (error) {
//         res.status(500).send(error.message);
//       } else {
//         console.log(book);
//         res.status(200).send(book);
//       }
//     }
//   );
// });
// router.delete('/:id', (req, res) => {
//   Book.findByIdAndRemove(
//     {
//       _id: req.params.id,
//     },
//     (error, success) => {
//       if (error) {
//         res.status(500).send(error.message);
//       } else {
//         console.log(success);
//         res.status(200).send(success);
//       }
//     }
//   );
// });

// const mapToBook = book => {
//   return {
//     id: book.id,
//     pavadinimas: book.title,
//     autorius: book.author,
//     kategorija: book.category,
//     suformuotaData: moment(book.year).format('MMMM Do YYYY, h:mm:ss a')
//   }
// };

module.exports = router;
