const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const moment = require('moment');
const app = express();
const port = process.env.PORT || 3000;

const databaseUser = 'vartotojas';
const databasePassword = 'vartotojas123';
const databaseName = 'car-rent-database';
const databasePort = 47537;

moment.locale('lt');

const carsRoutes = require('./routes/cars');

app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// database config
mongoose.connect(
  `mongodb://${databaseUser}:${databasePassword}@ds147537.mlab.com:${databasePort}/${databaseName}`,
  { useNewUrlParser: true }
);

app.get('/', (req, res) => res.send('Automobiliu nuoma'));
app.use('/cars', carsRoutes);

app.listen(port, () =>
  console.log(`Car rent API application listening on port ${port}!`)
);