const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = new Schema({
  mark: String,
  model: String,
  fueltype: String,
  gearbox: String,
  numberOfDoors: String,
  numberOfSeats: String,
  extras: String,
});

module.exports = mongoose.model('Car', CarSchema);
